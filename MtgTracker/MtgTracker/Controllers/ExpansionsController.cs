﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MtgTracker.Data;
using MtgTracker.Models;

namespace MtgTracker.Controllers
{
    public class ExpansionsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ExpansionsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Expansions
        public async Task<IActionResult> Index()
        {
            return View(await _context.Expansion.ToListAsync());
        }

        // GET: Expansions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var expansion = await _context.Expansion
                .FirstOrDefaultAsync(m => m.Id == id);
            if (expansion == null)
            {
                return NotFound();
            }

            return View(expansion);
        }

        // GET: Expansions/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Expansions/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Code,ReleaseDate,WasStandardLegal")] Expansion expansion)
        {
            if (ModelState.IsValid)
            {
                _context.Add(expansion);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(expansion);
        }

        // GET: Expansions/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var expansion = await _context.Expansion.FindAsync(id);
            if (expansion == null)
            {
                return NotFound();
            }
            return View(expansion);
        }

        // POST: Expansions/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Code,ReleaseDate,WasStandardLegal")] Expansion expansion)
        {
            if (id != expansion.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(expansion);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ExpansionExists(expansion.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(expansion);
        }

        // GET: Expansions/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var expansion = await _context.Expansion
                .FirstOrDefaultAsync(m => m.Id == id);
            if (expansion == null)
            {
                return NotFound();
            }

            return View(expansion);
        }

        // POST: Expansions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var expansion = await _context.Expansion.FindAsync(id);
            _context.Expansion.Remove(expansion);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ExpansionExists(int id)
        {
            return _context.Expansion.Any(e => e.Id == id);
        }
    }
}
