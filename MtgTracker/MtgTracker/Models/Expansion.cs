﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MtgTracker.Models
{
    public class Expansion
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public DateTime ReleaseDate { get; set; }
        public bool WasStandardLegal { get; set; }
        public Expansion()
        {

        }
    }
}
